local system = require 'pandoc.system'

local tikz_doc_template = [[
\documentclass{standalone}
\usepackage{import}
\usepackage{xifthen}
\usepackage{pdfpages}
\usepackage{transparent}
\usepackage{xcolor}
\usepackage{wrapfig}
\usepackage{tabularx}
\usepackage{array, ragged2e}
\usepackage[none]{hyphenat}
\usepackage{paracol}
\usepackage{tikz,pgf}
\usepackage{pgfplots}
\usepackage{karnaugh-map}
\usepackage{enumitem}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{bm}
\usepackage{float}

\newlength\textsize
\makeatletter
    \setlength{\textsize}{\f@size pt}
\makeatother

\pgfdeclarelayer{background}
\pgfdeclarelayer{foreground}
\pgfsetlayers{background,main,foreground}

\newcolumntype{P}[1]{>{\RaggedRight\arraybackslash}p{#1}}

\newcolumntype{C}[1]{>{\centering\arraybackslash}p{#1}}

\newcommand{\incfig}[2]{
	\def\svgwidth{#2}
	\import{./figures/}{#1.pdf_tex}
}

\usetikzlibrary{arrows,shapes,automata,petri,positioning,calc,arrows.meta,shapes,backgrounds,fit,scopes}

\tikzset{
    statediag-place/.style={
        circle,
        thick,
        draw=black,
        fill=gray!50,
        minimum size=6mm,
    },
    cnode/.style={
        circle,
        thick,
        draw=black,
        minimum size=6mm,
    },
    zlevel/.style={
        execute at begin scope={\pgfonlayer{#1}},
        execute at end scope={\endpgfonlayer}
    }
}


\newcommand{\edge}[4]{\path[->] (#1) edge [#2] node {#4} (#3);}

\begin{document}
\nopagecolor
\pgfsetlayers{background,main,foreground}
%s
\end{document}
]]

local function tikz2image(src, filetype, outfile)
  system.with_temporary_directory('tikz2image', function (tmpdir)
    system.with_working_directory(tmpdir, function()
      local f = io.open('tikz.tex', 'w')
      f:write(tikz_doc_template:format(src))
      f:close()
      os.execute('pdflatex tikz.tex')
      if filetype == 'pdf' then
        os.rename('tikz.pdf', outfile)
      elseif filetype == 'png' then
        os.execute('convert -quiet -density 800 -quality 90 tikz.pdf ' .. outfile)
        --os.execute('pdftoppm -png tikz.pdf > ' .. outfile)
      else
        os.execute('pdf2svg tikz.pdf ' .. outfile)
      end
    end)
  end)
end

extension_for = {
  docx = 'png',
  html = 'svg',
  html4 = 'svg',
  html5 = 'svg',
  latex = 'pdf',
  beamer = 'pdf' }

local function file_exists(name)
  local f = io.open(name, 'r')
  if f ~= nil then
    io.close(f)
    return true
  else
    return false
  end
end

local function has_tikz(str)
    return str:find([[\begin{tikzpicture}]]) and true or false
end

local function extract_tikz(str)
    local s = str:find([[\begin{tikzpicture}]])
    local _,e = str:find([[\end{tikzpicture}]])
    return str:sub(s,e)
end

function RawBlock(el)
  if has_tikz(el.text) then
    local tikzText = extract_tikz(el.text)
    local filetype = extension_for[FORMAT] or 'png'
    local fbasename = pandoc.sha1(tikzText) .. '.' .. filetype
    local fname = system.get_working_directory() .. '/' .. fbasename
    if not file_exists(fname) then
      tikz2image(tikzText, filetype, fname)
    end
    return pandoc.Para({pandoc.Image({}, fbasename)})
  else
   return el
  end
end
