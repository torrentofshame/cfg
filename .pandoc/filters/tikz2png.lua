--\\usepackage{tikz}\n\\usepackage{stanli}\n

--[====[
local notes_header = io.open("/home/torrentofshame/.pandoc/resources/notes_header.tex", "r")

local note_template = notes_header:read("a")

local doc_template = ([[
\documentclass{standalone}
]]
.. note_template ..
[[
\begin{document}
%s
\end{document}
]])

notes_header:close()
--]====]

local doc_template = [[
\documentclass{standalone}
\usepackage{import}
\usepackage{xifthen}
\usepackage{pdfpages}
\usepackage{transparent}
\usepackage{xcolor}
\usepackage{wrapfig}
\usepackage{tabularx}
\usepackage{array, ragged2e}
\usepackage[none]{hyphenat}
\usepackage{paracol}
\usepackage{tikz,pgf}
\usepackage{pgfplots}
\usepackage{karnaugh-map}
\usepackage{enumitem}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{bm}
\usepackage{float}

\newlength\textsize
\makeatletter
    \setlength{\textsize}{\f@size pt}
\makeatother

\pgfdeclarelayer{background}
\pgfdeclarelayer{foreground}
\pgfsetlayers{background,main,foreground}

\newcolumntype{P}[1]{>{\RaggedRight\arraybackslash}p{#1}}

\newcolumntype{C}[1]{>{\centering\arraybackslash}p{#1}}

\newcommand{\incfig}[2]{
	\def\svgwidth{#2}
	\import{./figures/}{#1.pdf_tex}
}

\usetikzlibrary{arrows,shapes,automata,petri,positioning,calc,arrows.meta,shapes,backgrounds,fit,scopes}

\tikzset{
    statediag-place/.style={
        circle,
        thick,
        draw=black,
        fill=gray!50,
        minimum size=6mm,
    },
    cnode/.style={
        circle,
        thick,
        draw=black,
        minimum size=6mm,
    },
    zlevel/.style={
        execute at begin scope={\pgfonlayer{#1}},
        execute at end scope={\endpgfonlayer}
    }
}


\newcommand{\edge}[4]{\path[->] (#1) edge [#2] node {#4} (#3);}

\newenvironment{statediagram}
{
\begin{tikzpicture}
}
{
\end{tikzpicture}
}

\begin{document}
%s
\end{document}
]]

--- Create a standalone LaTeX document which contains only the TikZ picture.
--- Convert to png via Imagemagick.
local function tikz2image(src, outfile)
  local tmp = os.tmpname()
  local tmpdir = string.match(tmp, "^(.*[\\/])") or "."
  local f = io.open(tmp .. ".tex", 'w')
  f:write(doc_template.format(src))
  f:close()
  os.execute("pdflatex -output-directory " .. tmpdir  .. " " .. tmp)
  os.execute("convert " .. tmp .. ".pdf " .. outfile)
  os.remove(tmp .. ".tex")
  os.remove(tmp .. ".pdf")
  os.remove(tmp .. ".log")
  os.remove(tmp .. ".aux")
end

local function file_exists(name)
  local f = io.open(name, 'r')
  if f ~= nil then io.close(f); return true
  else return false end
end

function RawBlock(el)
  -- Don't alter element if it's not a tikzpicture environment
  if not el.text:match'^\\begin{tikzpicture}' then
    return nil
    -- Alternatively, parse the contained LaTeX now:
    -- return pandoc.read(el.text, 'latex').blocks
  end 
  local fname = pandoc.sha1(el.text) .. ".png"
  if not file_exists(fname) then
    tikz2image(el.text, fname)
  end
  return pandoc.Para({pandoc.Image({}, fname)})
end

