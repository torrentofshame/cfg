__git_repos()
{
    fd --base-directory $HOME/code-repos -d 4 -t d -H -I .git$ . | cut -c 3- | sed -s 's@/\.git/\?$@@'
}

# TODO: This contains redundant code from ~/.local/bin/code_repos_rofi. <- make this a lie (but also
# have both scripts still function properly)

__fzf_code_repos__() {
    local dir
    dir=$( (echo new; __git_repos) | fzf --height 40% --reverse )

    if [[ x"new" = x"${dir}" ]]; then
        echo "TODO: enter a tui asking info on setting up new repo/cloning"
        cd "$HOME/code-repos"
    elif [[ -d "${HOME}/code-repos/${REPO}" ]]; then
        cd "$HOME/code-repos/$dir"
    else
        NEW_DIR="$HOME/code-repos/$REPO"
        mkdir -p $NEW_DIR
        cd $NEW_DIR
    fi
}
