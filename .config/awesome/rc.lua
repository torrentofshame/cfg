-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

-- Standard awesome library
local screen = screen
local gears = require("gears")
local awful = require("awful")
require("awful.remote")
require("awful.autofocus")
-- Widget and layout library
local wibox = require("wibox")
-- Theme handling library
local beautiful = require("beautiful")
-- Notification library
local naughty = require("naughty")
local menubar = require("menubar")
require("config.hotkeys_popup")
local hotkeys_popup = require("awful.hotkeys_popup")
-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")
--local lain = require("lain")
local freedesktop = require("freedesktop")
local type = type
local gtable = require("gears.table")
local gmath = require("gears.math")
local gdebug = require("gears.debug")

local isLaptop = os.getenv("WHICHPOOTER") == "laptop"

local ascreen = require("awful.screen")
local capi = {
    screen = screen,
    mouse = mouse,
    awesome = awesome,
    client = client,
    tag = tag
}

local isLaptop = os.getenv("WHICHPOOTER") == "laptop"

-- Import configs
local config = require("config")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
    naughty.notify{
        preset = naughty.config.presets.critical,
        title = "Oops, there were errors during startup!",
        text = awesome.startup_errors
    }
end

-- Handle runtime errors after startup
do
    local in_error = false
    awesome.connect_signal("debug::error", function (err)
        -- Make sure we don't go into an endless error loop
        if in_error then return end
        in_error = true

        naughty.notify{
            preset = naughty.config.presets.critical,
            title = "Oops, an error happened!",
            text = tostring(err)
        }
        in_error = false
    end)
end
-- }}}


--[[ Add titlebars only to floating windows ]]
client.connect_signal("property::floating",
    function (c)
        awful.titlebar.hide(c)
        --[[ on second thought, I don't need titlebars at all
        if c.floating and not(c.maximized) and c.name ~= "Picture in picture" then
            awful.titlebar.show(c)
        else
            awful.titlebar.hide(c)
        end
        --]]
    end
)

-- {{{ Variable definitions
-- Themes define colours, icons, font and wallpapers.
beautiful.init(config.theme_path)

-- This is used later as the default terminal and editor to run.
--local webbrowser = "google-chrome"
local terminal = config.terminal
local editor = config.editor
local ModKey = config.modkey

-- Table of layouts to cover with awful.layout.inc, order matters.
awful.layout.layouts = {
    --awful.layout.suit.floating,
    awful.layout.suit.tile.left,
    awful.layout.suit.tile,
    awful.layout.suit.tile.bottom,
    --awful.layout.suit.tile.top,
    awful.layout.suit.fair,
    --awful.layout.suit.fair.horizontal,
    --awful.layout.suit.spiral,
    --awful.layout.suit.spiral.dwindle,
    --awful.layout.suit.max,
    --awful.layout.suit.max.fullscreen,
    awful.layout.suit.magnifier,
    --awful.layout.suit.corner.nw,
    -- awful.layout.suit.corner.ne,
    -- awful.layout.suit.corner.sw,
    -- awful.layout.suit.corner.se,
}
-- }}}
local function get_screen(s)
    return s and capi.screen[s]
end

awful.layout.dyn_layouts = {
    awful.layout.layouts,
    {
        --awful.layout.suit.tile,
        awful.layout.suit.tile.top,
        awful.layout.suit.tile.bottom,
        awful.layout.suit.fair.horizontal
    }
}
local dyn_layouts = awful.layout.dyn_layouts

awful.layout.get_tag_layout_index = function(t)
    return gtable.hasitem(dyn_layouts[t.screen.index], t.layout)
end

local function is_landscape(width, height)
    -- square monitors will count as landscape!
    return width >= height
end

awful.layout.inc = function (i, s, layouts)
    if type(i) == "table" then
        -- Older versions of this function had arguments (layouts, i, s), but
        -- this was changed so that 'layouts' can be an optional parameter
        gdebug.deprecate("Use awful.layout.inc(increment, screen, layouts) instead"..
            " of awful.layout.inc(layouts, increment, screen)", {deprecated_in=5})

        layouts, i, s = i, s, layouts
    end
    s = get_screen(s or ascreen.focused())

    local t = s.selected_tag

    if not t then return end

    -- by default use the screen index
    local this_layout = dyn_layouts[s.index or 1]
    -- landscape monitors should use 1st layouts
    if is_landscape(s.geometry.width, s.geometry.height) then
        this_layout = dyn_layouts[1]
    -- portrait monitors should use 2nd layouts
    else
        this_layout = dyn_layouts[2]
    end

    layouts = this_layout or layouts or t.layouts or {}

    --layouts = layouts or t.layouts or {}

    if #layouts == 0 then
        layouts = awful.layout.layouts
    end

    local cur_l = awful.layout.get(s)

    -- First try to match the object
    local cur_idx =  gtable.find_first_key(
        layouts, function(_, v) return v == cur_l or cur_l._type == v end, true
    )

    -- Safety net: handle cases where another reference of the layout
    -- might be given (e.g. when (accidentally) cloning it).
    cur_idx = cur_idx or gtable.find_first_key(
        layouts, function(_, v) return v.name == cur_l.name end, true
    )

    -- Trying to come up with some kind of fallback layouts to iterate would
    -- never produce a result the user expect, so if there is nothing to
    -- iterate over, do not iterate.
    if not cur_idx then return end

    local newindex = gmath.cycle(#layouts, cur_idx + i)
    awful.layout.set(layouts[newindex], t)
end

-- {{{ Menu
-- Create a launcher widget and a main menu
local myawesomemenu = {
   { "hotkeys", function() return false, hotkeys_popup.show_help end },
   { "manual", terminal .. " -e man awesome" },
   { "edit config", string.format("%s -e %s %s", terminal, editor, awesome.conffile) },
   { "restart", awesome.restart },
   { "quit", function() awesome.quit() end },
}

awful.util.mymainmenu = freedesktop.menu.build{
    icon_size = beautiful.menu_height or 16,
    before = {
        { "Awesome", myawesomemenu, "/usr/share/awesome/icons/awesome16.png" }
    },
    after = {
        { "Open terminal", terminal, "/usr/share/pixmaps/Alacritty.svg" }
    }
}

-- Menubar configuration
menubar.utils.terminal = terminal -- Set the terminal for applications that require it
-- }}}


-- Create a wibox for each screen and add it
awful.util.taglist_buttons = gears.table.join(
    awful.button({ }, 1, function(t) t:view_only() end),

    awful.button({ ModKey }, 1,
        function(t)
            if client.focus then
                client.focus:move_to_tag(t)
            end
        end
    ),

    awful.button({ }, 3, awful.tag.viewtoggle),

    awful.button({ ModKey }, 3,
        function(t)
            if client.focus then
                client.focus:toggle_tag(t)
            end
        end
    ),

    awful.button({ }, 4, function(t) awful.tag.viewnext(t.screen) end),

    awful.button({ }, 5, function(t) awful.tag.viewprev(t.screen) end)
)

awful.util.tasklist_buttons = gears.table.join(
    awful.button({ }, 1,
        function (c)
            if c == client.focus then
                c.minimized = true
            else
                c:emit_signal(
                    "request::activate",
                    "tasklist",
                    {raise = true}
                )
            end
        end
    ),

    awful.button({ }, 3,
        function ()
            awful.menu.client_list({ theme = { width = 250 } })
        end
    ),

    awful.button({ }, 4,
        function ()
            awful.client.focus.byidx(1)
        end
    ),

    awful.button({ }, 5,
        function ()
            awful.client.focus.byidx(-1)
        end
    )
)

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry",
    function (s)
        -- Wallpaper
        if beautiful.wallpaper then
            local wallpaper = beautiful.wallpaper
            -- If wallpaper is a function, call it with the screen
            if type(wallpaper) == "function" then
                wallpaper = wallpaper(s)
            end
            gears.wallpaper.maximized(wallpaper, s, true)
        end
    end
)

awful.screen.connect_for_each_screen(function(s) beautiful.at_screen_connect(s) end)
-- }}}

--[[ Import our keybindings ]]
local bindings = config.bindings

root.buttons(bindings.mouse.global)
root.keys(bindings.global)

-- {{{ Rules
-- Rules to apply to new clients (through the "manage" signal).
-- }}}
awful.rules.rules = config.rules

-- {{{ Signals
-- Signal function to execute when a new client appears.
client.connect_signal("manage", function (c)
    -- Set the windows at the slave,
    -- i.e. put it at the end of others instead of setting it master.
    -- if not awesome.startup then awful.client.setslave(c) end

    --[[ Force all GnuCash popups to be Floating clients. ]]
    if c.instance == "gnucash"
      and c.class == "Gnucash"
      and c.name:match("GnuCash$") == nil then
        c.floating = true
    end

--[[
    if c.instance == "eww" or c.instance == "Eww" then
        c.below = true
        c.skip_taskbar = true
        c.floating = true
    end
    --]]

    if awesome.startup
      and not c.size_hints.user_position
      and not c.size_hints.program_position then
        -- Prevent clients from being unreachable after screen count changes.
        awful.placement.no_offscreen(c)
    end
    c.shape = function(cr,w,h)
        gears.shape.rounded_rect(cr,w,h,6)
    end
end)

client.connect_signal("request::titlebars", config.titlebars)

-- Enable sloppy focus, so that focus follows mouse.
client.connect_signal("mouse::enter", function(c)
    c:emit_signal("request::activate", "mouse_enter", {raise = false})
end)

client.connect_signal("focus", function(c) c.border_color = beautiful.border_focus end)
client.connect_signal("unfocus", function(c) c.border_color = beautiful.border_normal end)
-- }}}

--[====[
do
    awful.spawn.with_shell([[picom]])
    awful.spawn.with_shell([[mpv --no-video "/home/torrentofshame/.local/share/sounds/winxp.mp3"]])
    --awful.spawn.once([[play-wallpaper /home/torrentofshame/.local/share/backgrounds/live/asteroids.mp4]])
end
--]====]

-- Autostart
awful.spawn.with_shell(
    'if (xrdb -query | grep -q "^awesome\\.started:\\s*true$"); then exit; fi;' ..
    'xrdb -merge <<< "awesome.started:true";' ..
    -- list each of your autostart commands, followed by ; inside single quotes, followed by ..
    'dex --environment Awesome --autostart --search-paths "${XDG_CONFIG_DIRS:-/etc/xdg}/autostart:${XDG_CONFIG_HOME:-$HOME/.config}/autostart";' -- https://github.com/jceb/dex
)

local monitorAudioDev = 'alsa/hdmi:CARD=NVidia,DEV=0'
local dtopAudioArg = ''
if not isLaptop then
    dtopAudioArg = ' --audio-device=' .. monitorAudioDev
end

awful.spawn.with_shell(
    'if (xrdb -query | grep -q "^awesome\\.started:\\s*true$"); then exit; fi;' ..
    'sleep 1 && ' ..
    'mpv --no-video "/home/torrentofshame/.local/share/sounds/winxp.mp3"'.. dtopAudioArg ..';'
)

awful.spawn.with_shell(
    'if (xrdb -query | grep -q "^awesome\\.started:\\s*true$"); then exit; fi;' ..
    'picom -b;'
)

-- Check if xbps update check script gave us results
local xbps_update_check_file = "/tmp/xbps-update-boot-count"
if gears.filesystem.file_readable(xbps_update_check_file) then
    local f = io.open(xbps_update_check_file)
    if f then
        local updatecnt = f:read("n")
        if updatecnt then

            naughty.notify{
                title="Xbps Updates",
                text="Updates are available for ".. tostring(updatecnt) .." xbps packages!"
            }

        end
        f:close()

        os.remove(xbps_update_check_file)
    end
end

gears.timer {
    timeout = 30,
    autostart = true,
    callback = function()
        collectgarbage()
    end
}
