---------------------------
-- Default awesome theme --
---------------------------

local theme_assets = require("beautiful.theme_assets")
local xresources = require("beautiful.xresources")
local dpi = xresources.apply_dpi
local gears = require("gears")
local wibox = require("wibox")
local awful = require("awful")
local beautiful = require("beautiful")
--local lain = require("lain")
local volume_widget = require("awesome-wm-widgets.volume-widget.volume")
local fs_widget = require("awesome-wm-widgets.fs-widget.fs-widget")
local net_speed_widget = require("awesome-wm-widgets.net-speed-widget.net-speed")
local logout_menu_widget = require("awesome-wm-widgets.logout-menu-widget.logout-menu")
local calendar_widget = require("awesome-wm-widgets.calendar-widget.calendar")
local cpu_widget = require("widgets.cpu-widget.cpu-widget")
local github_contributions_widget = require("awesome-wm-widgets.github-contributions-widget.github-contributions-widget")
--[[
local github_prs_widget = require("awesome-wm-widgets.github-prs-widget")
local github_activity_widget = require("awesome-wm-widgets.github-activity-widget.github-activity-widget")
local jira_widget = require("awesome-wm-widgets.jira-widget.jira")
--]]
local brightness_widget = require("awesome-wm-widgets.brightness-widget.brightness")

local theme = {}

theme.dir = os.getenv("HOME") .. "/.config/awesome/themes/stolenthemelol"

local isLaptop = os.getenv("WHICHPOOTER") == "laptop"

if isLaptop then
    theme.font          = "sans 8"
else
    theme.font          = "sans 5"
end

theme.opacity_normal = 0.95
theme.opacity_active = 1

theme.notification_shape = gears.shape.rounded_rect
theme.hotkeys_shape = gears.shape.rounded_rect
beautiful.tooltip_shape = gears.shape.rounded_rect

theme.bg_normal     = "#222222"
theme.bg_focus      = "#535d6c"
theme.bg_urgent     = "#ff0000"
theme.bg_minimize   = "#444444"
theme.bg_systray    = theme.bg_normal

theme.fg_normal     = "#aaaaaa"
theme.fg_focus      = "#ffffff"
theme.fg_urgent     = "#ffffff"
theme.fg_minimize   = "#ffffff"

theme.useless_gap   = 4
theme.gap_single_client = true
theme.border_width  = 1
theme.border_normal = "#000000"
theme.border_focus  = "#535d6c"
theme.border_marked = "#91231c"

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- taglist_[bg|fg]_[focus|urgent|occupied|empty|volatile]
-- tasklist_[bg|fg]_[focus|urgent]
-- titlebar_[bg|fg]_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- mouse_finder_[color|timeout|animate_timeout|radius|factor]
-- prompt_[fg|bg|fg_cursor|bg_cursor|font]
-- hotkeys_[bg|fg|border_width|border_color|shape|opacity|modifiers_fg|label_bg|label_fg|group_margin|font|description_font]
-- Example:
--theme.taglist_bg_focus = "#ff0000"

if isLaptop then
    theme.hotkeys_font = "Ubuntu Nerd Font Bold 8"
else
    theme.hotkeys_font = "Ubuntu Nerd Font Bold 10"
end

--theme.hotkeys_description_font = "sans 6"
theme.hotkeys_modifiers_fg = "#808080"

-- Generate taglist squares:
local taglist_square_size = dpi(4)
theme.taglist_squares_sel = theme_assets.taglist_squares_sel(
    taglist_square_size, theme.fg_normal
)
theme.taglist_squares_unsel = theme_assets.taglist_squares_unsel(
    taglist_square_size, theme.fg_normal
)

-- Variables set for theming notifications:
-- notification_font
-- notification_[bg|fg]
-- notification_[width|height|margin]
-- notification_[border_color|border_width|shape|opacity]

-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_submenu_icon = theme.dir .. "/submenu.png"
theme.menu_height = dpi(15)
theme.menu_width  = dpi(100)

-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.bg_widget = "#cc0000"

-- Define the image to load
theme.titlebar_close_button_normal = theme.dir .. "/titlebar/close_normal.png"
theme.titlebar_close_button_focus  = theme.dir .. "/titlebar/close_focus.png"

theme.titlebar_minimize_button_normal = theme.dir .. "/titlebar/minimize_normal.png"
theme.titlebar_minimize_button_focus  = theme.dir .. "/titlebar/minimize_focus.png"

theme.titlebar_ontop_button_normal_inactive = theme.dir .. "/titlebar/ontop_normal_inactive.png"
theme.titlebar_ontop_button_focus_inactive  = theme.dir .. "/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_active = theme.dir .. "/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_active  = theme.dir .. "/titlebar/ontop_focus_active.png"

theme.titlebar_sticky_button_normal_inactive = theme.dir .. "/titlebar/sticky_normal_inactive.png"
theme.titlebar_sticky_button_focus_inactive  = theme.dir .. "/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_active = theme.dir .. "/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_active  = theme.dir .. "/titlebar/sticky_focus_active.png"

theme.titlebar_floating_button_normal_inactive = theme.dir .. "/titlebar/floating_normal_inactive.png"
theme.titlebar_floating_button_focus_inactive  = theme.dir .. "/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_active = theme.dir .. "/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_active  = theme.dir .. "/titlebar/floating_focus_active.png"

theme.titlebar_maximized_button_normal_inactive = theme.dir .. "/titlebar/maximized_normal_inactive.png"
theme.titlebar_maximized_button_focus_inactive  = theme.dir .. "/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_active = theme.dir .. "/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_active  = theme.dir .. "/titlebar/maximized_focus_active.png"

theme.wallpaper = theme.dir .. "/void-wallpaper.png"
--theme.wallpaper = "/home/torrentofshame/.local/share/backgrounds/planets.jpg"

-- You can use your own layout icons like this:
theme.layout_fairh = theme.dir .. "/layouts/fairhw.png"
theme.layout_fairv = theme.dir .. "/layouts/fairvw.png"
theme.layout_floating  = theme.dir .. "/layouts/floatingw.png"
theme.layout_magnifier = theme.dir .. "/layouts/magnifierw.png"
theme.layout_max = theme.dir .. "/layouts/maxw.png"
theme.layout_fullscreen = theme.dir .. "/layouts/fullscreenw.png"
theme.layout_tilebottom = theme.dir .. "/layouts/tilebottomw.png"
theme.layout_tileleft   = theme.dir .. "/layouts/tileleftw.png"
theme.layout_tile = theme.dir .. "/layouts/tilew.png"
theme.layout_tiletop = theme.dir .. "/layouts/tiletopw.png"
theme.layout_spiral  = theme.dir .. "/layouts/spiralw.png"
theme.layout_dwindle = theme.dir .. "/layouts/dwindlew.png"
theme.layout_cornernw = theme.dir .. "/layouts/cornernww.png"
theme.layout_cornerne = theme.dir .. "/layouts/cornernew.png"
theme.layout_cornersw = theme.dir .. "/layouts/cornersww.png"
theme.layout_cornerse = theme.dir .. "/layouts/cornersew.png"

-- Generate Awesome icon:
theme.awesome_icon = theme_assets.awesome_icon(
    theme.menu_height, theme.bg_focus, theme.fg_focus
)

-- Define the icon theme for application icons. If not set then the icons
-- from /usr/share/icons and /usr/share/icons/hicolor will be used.
theme.icon_theme = nil

-- {{{ Wibar
-- Create a textclock widget
local mytextclock = wibox.widget.textclock(
    "%a %b %d, %l:%M %P "
)

local cw = calendar_widget{
    theme = "dark",
    start_sunday = true,
    placement = "top_right"
}
mytextclock:connect_signal("button::press",
    function (_, _, _, button)
        if button == 1 then cw.toggle() end
    end
)

local bat, bright
if isLaptop then
    local batteryarc_widget = require("awesome-wm-widgets.batteryarc-widget.batteryarc")

    bright = brightness_widget{
        program = "brightnessctl",
        tooltip = true,
        base = 30,
    }
    bat = batteryarc_widget{
        show_current_level = true,
        arc_thickness = 1
    }
end

function theme.at_screen_connect(s)
    -- Wallpaper
    gears.wallpaper.maximized(theme.wallpaper, s)

    -- Each screen has its own tag table.
    awful.tag({ "1", "2", "3", "4", "5", "6", "7", "8", "9" }, s,
        awful.layout.dyn_layouts[s.index][1])

    -- Create a promptbox for each screen
    s.mypromptbox = awful.widget.prompt()
    -- Create an imagebox widget which will contain an icon indicating which layout we're using.
    -- We need one layoutbox per screen.
    s.mylayoutbox = awful.widget.layoutbox(s)
    s.mylayoutbox:buttons(gears.table.join(
                           awful.button({ }, 1, function () awful.layout.inc( 1) end),
                           awful.button({ }, 3, function () awful.layout.inc(-1) end),
                           awful.button({ }, 4, function () awful.layout.inc( 1) end),
                           awful.button({ }, 5, function () awful.layout.inc(-1) end)))
    -- Create a taglist widget
    s.mytaglist = awful.widget.taglist {
        screen  = s,
        filter  = awful.widget.taglist.filter.all,
        buttons = awful.util.taglist_buttons
    }

    -- Create a tasklist widget
    s.mytasklist = awful.widget.tasklist {
        screen  = s,
        filter  = awful.widget.tasklist.filter.currenttags,
        buttons = awful.util.tasklist_buttons
    }

    -- Separators
    local spr = wibox.widget.textbox('   ')
    local slspr = wibox.widget.textbox(' ')

    local sgeo = s.geometry
    -- Create the wibox
    s.mywibox = awful.wibar({
        position = "top",
        screen = s,
        bg = theme.bg_normal,
        fg = theme.fg_normal,
        width = sgeo.width - 16,
        height = 24, y = sgeo.y + 4, x = sgeo.x + 8,
        shape = gears.shape.rounded_bar,
        border_width = theme.border_width,
        border_color = theme.border_normal,
        visible = true
    })

    local volume = volume_widget{
        widget_type = "arc",
        device = "pipewire",
        mixer_cmd = "helvum"
    }
    local mylauncher = awful.widget.launcher{
        image = beautiful.awesome_icon,
        menu = awful.util.mymainmenu
    }

    local sep = wibox.widget {
        forced_width = 3,
        widget = awful.widget.separator
    }

    -- Keyboard map indicator and switcher
    local mykeyboardlayout = awful.widget.keyboardlayout()
    local gh_contrib = github_contributions_widget{
        username = "TorrentofShame",
        theme = "leftpad"
    }
--[[
    local gh_prs = github_prs_widget{
        reviewer = "TorrentofShame",
    }
    local gh_act = github_activity_widget{
        username = "TorrentofShame",
    }

    local jira = jira_widget{
        host = "https://lokioapp.atlassian.net"
    }
--]]

    local net = net_speed_widget()
    local cpu = cpu_widget{
        is_hybrid_core = not(isLaptop)
    }
    local fs = fs_widget{
        mounts = isLaptop and { '/', '/home' } or { '/', '/media/backup', '/media/data'},
        popup_bg = beautiful.bg_normal
    }
    local logout = logout_menu_widget()

    -- desktop brightness
    if not(isLaptop) then
        bright = brightness_widget{
            program = "ddcutil",
            displayIdx = 2,
            tooltip = true,
            base = 30,
        }
    end

    local dyn_right_widgets = {
        { -- Screen 1
            layout = wibox.layout.fixed.horizontal,
            sep,
            gh_contrib,
            --gh_act,
            --gh_prs,
            --jira,
            net,
            mykeyboardlayout,
            wibox.widget.systray(),
            sep,
            volume,
            sep,
            bright,
            sep,
            cpu,
            fs,
            mytextclock,
            bat,
            logout,
            wibox.container.margin(s.mylayoutbox,0,2,0,0),
            slspr
        },
        { -- Screen 2
            layout = wibox.layout.fixed.horizontal,
            sep,
            net,
            mykeyboardlayout,
            wibox.widget.systray(),
            sep,
            volume,
            sep,
            bright,
            sep,
            cpu,
            fs,
            mytextclock,
            bat,
            logout,
            wibox.container.margin(s.mylayoutbox,0,2,0,0),
            slspr
        }
    }

    -- Add widgets to the wibox
    s.mywibox:setup {
        layout = wibox.layout.align.horizontal,
        { -- Left widgets
            layout = wibox.layout.fixed.horizontal,
            --mylauncher,
            wibox.container.margin(s.mytaglist,10,0,0,0),
            s.mypromptbox,
        },
        s.mytasklist, -- Middle widget
        -- Right widgets
        dyn_right_widgets[s.index] or dyn_right_widgets[1]
    }

    s.mywibox:struts{
        left = 0,
        right = 0,
        top = 30,
        bottom = 0
    }
end

return theme

-- vim: filetype=lua:expandtab:shiftwidth=4:tabstop=8:softtabstop=4:textwidth=80
