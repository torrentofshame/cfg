local hotkeys_popup = require("awful.hotkeys_popup.widget")

local obsidian_rule = { class = { "obsidian" } }
for group_name, group_data in pairs({
    ["Obsidian: test"] = { color = "#483699", rule_any = obsidian_rule }
}) do
    hotkeys_popup.add_group_rules(group_name, group_data)
end

local obsidian_keys = {
    ["Obsidian: test"] = {{
        modifiers = { },
        keys = {
            F2="Edit file title"
        }
    }, {
        modifiers = { "Alt" },
        keys = {
            l="Add internal link",
            t="Open today's daily note",
            Enter="Follow link under cursor",
            n="Create new note from template",
        }
    }, {
        modifiers = {"Ctrl"},
        keys = {
            w="Close active pane",
            p="Open command palette",
            b="Cycle bullet/checkbox",
            d="Delete paragraph",
            k="Insert Markdown link",
            o="Navigate back",
            i="Navigate forward",
            q="Open quick switcher",
            Enter="Toggle checkbox status"
        }
    }, {
        modifiers = { "Ctrl", "Shift" },
        keys = {
            n="Create note in new pane",
            s="Toggle Live Preview/Source mode",
            Enter="Open in default app"
        }
    }, {
        modifiers = { "Ctrl", "Alt" },
        keys = {
            Enter="Open link under cursor in new pane"
        }
    }}
}

hotkeys_popup.add_hotkeys(obsidian_keys)
