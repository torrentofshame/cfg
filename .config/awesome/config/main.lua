--[[
        Main config stuff goes here!
--]]

return {
    terminal = "alacritty",
    editor = "nvim",
    modkey = "Mod4",
    touchpadDevice = "ELAN1200:00 04F3:3058 Touchpad"
}
