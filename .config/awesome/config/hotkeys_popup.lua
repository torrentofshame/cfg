local widget = require("awful.hotkeys_popup.widget")
local gtable = require("gears.table")

local widget_instance = {
    hide_without_description = widget.hide_without_description,
    merge_duplicates = widget.merge_duplicates,
    group_rules = gtable.clone(widget.group_rules),
    labels = {
        Mod4="", -- Super
        Mod1="Alt",
        Shift="⇧", -- nil
        Escape="Esc",
        Insert="Ins",
        Delete="Del",
        Backspace="BackSpc",
        Ctrl="", -- nil
        Return="", -- Enter
        Next="PgDn",
        Prior="PgUp",
        ['#108']="Alt Gr",
        Left='←',
        Up='↑',
        Right='→',
        Down='↓',
        ['#67']="F1",
        ['#68']="F2",
        ['#69']="F3",
        ['#70']="F4",
        ['#71']="F5",
        ['#72']="F6",
        ['#73']="F7",
        ['#74']="F8",
        ['#75']="F9",
        ['#76']="F10",
        ['#95']="F11",
        ['#96']="F12",
        ['#10']="1",
        ['#11']="2",
        ['#12']="3",
        ['#13']="4",
        ['#14']="5",
        ['#15']="6",
        ['#16']="7",
        ['#17']="8",
        ['#18']="9",
        ['#19']="0",
        ['#20']="-",
        ['#21']="=",
        Control="" -- Ctrl
    },
    _additional_hotkeys = {},
    _cached_wiboxes = {},
    _cached_awful_keys = {},
    _colors_counter = {},
    _group_list = {},
    _widget_settings_loaded = false,
}

widget.default_widget = widget.new(widget_instance)

return widget
