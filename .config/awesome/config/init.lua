local moduleDirectory = ... .. "."

local config = {}

function config:compose(...)
    local args = {...}
    for _,tbl in ipairs(args) do
        for k,v in pairs(tbl) do
            self[k] = v
        end
    end
end

--[[-----------------------]]

local main = require(moduleDirectory .. "main")
local bindings = require(moduleDirectory .. "bindings")
local rules = require(moduleDirectory .. "rules")
local titlebars = require(moduleDirectory .. "titlebars")

config:compose(
    {
        theme_path = os.getenv("HOME") .. "/.config/awesome/themes/stolenthemelol/theme.lua",
        bindings = bindings,
        rules = rules,
        titlebars = titlebars
    },
    main
)

require(moduleDirectory .. "keys.obsidian")

return config
