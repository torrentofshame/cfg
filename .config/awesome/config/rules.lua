local beautiful = require("beautiful")
local bindings = require("config.bindings")
local awful = require("awful")

local rules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = awful.client.focus.filter,
                     raise = true,
                     keys = bindings.client,
                     buttons = bindings.mouse.client,
                     screen = awful.screen.preferred,
                     placement = awful.placement.no_overlap+awful.placement.no_offscreen,
                     titlebars_enabled = false,
                     skip_taskbar = false
     }
    },

    -- Evolution Shit
    { rule_any = {
        class = {
          "Evolution-alarm-notify"
        }
    }, properties = {
            floating = true,
            modal = true,
            placement = awful.placement.centered,
            width = 730, height = 430,
            opacity = 0.95,
        }
    },

    -- Floating clients.
    { rule_any = {
        instance = {
          "DTA",  -- Firefox addon DownThemAll.
          "copyq",  -- Includes session name in class.
          "pinentry",
        },
        class = {
          "Arandr",
          "Blueman-manager",
          "Gpick",
          "Kruler",
          "MessageWin",  -- kalarm.
          "Sxiv",
          "Tor Browser", -- Needs a fixed window size to avoid fingerprinting by screen size.
          "Wpa_gui",
          "veromix",
          "xtightvncviewer"},

        -- Note that the name property shown in xprop might be set slightly after creation of the client
        -- and the name shown there might not match defined rules here.
        name = {
          "Event Tester",  -- xev.
          -- fontforge
          "Problem explanation",
          "Point Info",
          "zoom",
          "Preferences"
        },
        role = {
          "AlarmWindow",  -- Thunderbird's calendar.
          "ConfigManager",  -- Thunderbird's about:config.
          "pop-up",       -- e.g. Google Chrome's (detached) Developer Tools.
        }
      }, properties = { floating = true }},

    -- "Floating" terminal (technically a sim of the magnifier layout but w/ 1 thing)
    -- Even though as of 2024-03-28T06:15:00-04:00, the --class cmdline option for alacritty
    -- works as --class <class>,<instance> w/ regards to how awesomewm reads a window's class and
    -- instance. This "truth", which does not make sense to have changed, has undoubtedly changed to
    -- --class <instance>,<class> and back again without warning.
    -- Therefore, I will now keep 2 versions of the below rule, to account for any future
    -- shenanigans from either Alacritty, AwesomeWM, or other unknown factor. (I blame dns)
    { rule = {
            instance = "FloatingTerminal",
            class = "Alacritty"
        },
        properties = {
            floating = true,
            width = 1000, height = 500,
            placement = awful.placement.centered
        }
    },

    { rule = {
        instance = "Alacritty",
        class = "FloatingTerminal"
        },
        properties = {
            floating = true,
            width = 1000, height = 500,
            placement = awful.placement.centered
        }
    },

    --[[ Android Emulator Stuff ]]
    { rule_any = {
            name = {
                "Emulator",
                "Extended Controls - .*",
                "Android Emulator - .*"
            }
        },
        properties = {
            floating = true,
        }
    },

    -- Add titlebars to normal clients and dialogs
    { rule_any = {type = { "normal", "dialog" }
      }, properties = { titlebars_enabled = false }
    },

    -- Picture-in-Picture
    { rule_any = {
            name = { "Picture in picture"--[[, "Joint State Publisher"--]] }
        },
        properties = {
            titlebars_enabled = false,
            floating = true,
            above = true,
            ontop = true,
            maximized = false,
            sticky = true,
            skip_taskbar = true
        }
    },

    { rule = {
            name = "Network Connections"
        },
        properties = {
            focus = true,
            floating = true,
            ["type"] = "utility",
            skip_taskbar = true,
            placement = awful.placement.no_offscreen+awful.placement.top_right
        }
    },

    { rule_any = {
            class = {
                "nm-connection-editor"
            }
        },
        properties = {
            focus = true,
            floating = true,
            ["type"] = "utility",
            skip_taskbar = true,
        }
    },

    { rule_any = {
            instance = {
                "pqiv"
            },
            class = {
                "RangeredPqiv"
            }
        },
        properties = {
            titlebars_enabled = false,
            floating = true,
            skip_taskbar = true,
            ["type"] = "dialog",
            focus = true
        }
    },

    -- Splash Screens that don't have the proper type set
    { rule_any = {
            name = "Discord Updater",
            ["type"] = "splash"
        },
        properties = {
            ["type"] = "splash"
        }
    },

    -- Force Globalprotect UI to popup near the taskbar where the icon is.
    { rule_any = {
        class = { "PanGPUI" },
        instance = { "PanGPUI" }
      },
      properties = {
        placement = awful.placement.no_offscreen+awful.placement.top_right,
        floating = true
      }
    },

    { rule = {
            class = "conky"
        },
        properties = {
            skip_taskbar = true
        }
    }

    --[[
    { rule = { class = "xournalpp" },
        properties = { tag = root.tags()[3], switchtotag = true }
    }
    --]]

    -- Set Firefox to always map on the tag named "2" on screen 1.
    -- { rule = { class = "Firefox" },
    --   properties = { screen = 1, tag = "2" } },
}

local isLaptop = os.getenv("WHICHPOOTER") == "laptop"

if (not(isLaptop)) then
    -- Slack and Discord should be opened on second monitor if exists
    -- TODO: Add thing to check for laptop shit
    rules[#rules+1] = { rule_any = {
            class = {
                "slack",
                "Slack",
                "discord"
            }
        },
        properties = {
            screen = "DP-0"
        }
    }
else
    rules[#rules+1] = { rule_any = {
            class = {
                "discord"
            }
        },
        properties = {
            tag = "1"
        }
    }
end

return rules
