local moduleDirectory = ... .. "."

return {
    mouse = require(moduleDirectory .. "mouse"),
    global = require(moduleDirectory .. "global"),
    client = require(moduleDirectory .. "client")
}
