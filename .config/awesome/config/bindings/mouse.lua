--[[
        Mouse Bindings
]]
local gears = require("gears")
local awful = require("awful")
local modkey = require("config.main").modkey


--[[ Global Mouse Bindings ]]
local global = gears.table.join(
    awful.button({ }, 3,
        function ()
            awful.util.mymainmenu:toggle()
        end
        )--,

--    awful.button({ }, 4, awful.tag.viewnext),

--    awful.button({ }, 5, awful.tag.viewprev)
)


--[[ Client Mouse Bindings ]]
local client = gears.table.join(
    awful.button({ }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
    end),
    awful.button({ modkey }, 1, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.move(c)
    end),
    awful.button({ modkey }, 3, function (c)
        c:emit_signal("request::activate", "mouse_click", {raise = true})
        awful.mouse.client.resize(c)
    end)
)


--[[-------------------]]
return {
    global = global,
    client = client
}
