au BufRead,BufNewFile Appfile set filetype=ruby
au BufRead,BufNewFile Fastfile set filetype=ruby
au BufRead,BufNewFile Matchfile set filetype=ruby
au BufRead,BufNewFile Pluginfile set filetype=ruby
