" Watch for Vimrc changes
augroup myvimrc
	au!
	au BufWritePost init.vim so $MYVIMRC | if has('gui_running') | so $MYVIMRC | endif
augroup END
set nocompatible

" for neovide
set guifont=UbuntuMono\ Nerd\ Font:h15

"set rtp+=~/.config/nvim/plugged/inkscape-figs

let $MYVIMLUA = "~/.config/nvim/lua/trnt"

let g:loaded_perl_provider = 0

" Vim-Plug
"call plug#begin('~/.config/nvim/plugged')
"
"Plug 'tweekmonster/startuptime.vim'
"
""Plug 'nathom/filetype.nvim'
"
"" Themes
"Plug 'torrentofshame/nvim-quantum'
"
"" StatusLine
"Plug 'nvim-lualine/lualine.nvim'
"
"" Telescope
""Plug 'nvim-lua/popup.nvim'
"Plug 'nvim-lua/plenary.nvim'
"Plug 'nvim-lua/popup.nvim'
""Plug 'fhill2/telescope-ultisnips.nvim'
""Plug 'nvim-telescope/telescope-media-files.nvim'
"
"Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
"Plug 'nvim-treesitter/playground'
"Plug 'nvim-treesitter/nvim-treesitter-context'
"Plug 'JoosepAlviste/nvim-ts-context-commentstring'
"Plug 'numToStr/Comment.nvim'
"
"Plug 'nvim-telescope/telescope.nvim', { 'branch': '0.1.x' }
"Plug 'nvim-telescope/telescope-fzy-native.nvim'
"Plug 'nvim-telescope/telescope-ui-select.nvim'
"
""Plug 'https://gitlab.com/torrentofshame/discord-rpc.nvim.git', { 'commit': '31a2aabdbd0ff322429d9498c678fffae2234ff6' }
"Plug '~/.config/nvim/plugged/discord-rpc.nvim'
"
"Plug 'Yggdroot/indentLine'
"Plug 'godlygeek/tabular' | Plug 'tpope/vim-markdown'
"Plug 'vim-pandoc/vim-pandoc'
"Plug 'vim-pandoc/vim-pandoc-syntax'
"
"" Vimtex
"Plug 'lervag/vimtex', { 'do': '~/.config/nvim/plug-patcher.sh vimtex &>/dev/null' }
"
"" Nvim LSP
"Plug 'neovim/nvim-lspconfig'
"Plug 'hrsh7th/cmp-nvim-lsp'
"Plug 'hrsh7th/cmp-buffer'
"Plug 'hrsh7th/cmp-path'
"Plug 'hrsh7th/cmp-cmdline'
"Plug 'kdheepak/cmp-latex-symbols'
"Plug 'hrsh7th/cmp-nvim-lsp-document-symbol'
"Plug 'hrsh7th/cmp-nvim-lsp-signature-help'
"Plug 'hrsh7th/nvim-cmp'
"Plug 'Dosx001/cmp-commit'
"Plug 'onsails/lspkind-nvim'
"Plug 'nvim-lua/lsp_extensions.nvim'
"
"Plug 'simrat39/rust-tools.nvim'
"
"Plug 'simrat39/symbols-outline.nvim'
"
"" Trouble.nvim
"Plug 'kyazdani42/nvim-web-devicons'
"Plug 'kyazdani42/nvim-tree.lua'
"
"" General Plugins
"Plug 'tpope/vim-surround'
""Plug 'SirVer/ultisnips' | Plug 'honza/vim-snippets', { 'rtp': '.' }
""Plug 'quangnguyen30192/cmp-nvim-ultisnips'
"Plug 'wakatime/vim-wakatime'
"Plug 'dense-analysis/ale'
"
"Plug 'tpope/vim-characterize'
"Plug 'mhinz/vim-signify'
"Plug 'tpope/vim-fugitive'
""Plug 'timuntersberger/neogit'
"
"" Always load vim-devicons as the last one
"Plug 'ryanoasis/vim-devicons'
"call plug#end()

" Instantiate Lua Configurations
lua require("trnt")

"===== Plugin Configurations ====="

" VimTex
let g:vimtex_view_method = 'zathura'
let g:vimtex_fold_enabled = 1

augroup THEFUCK
    au!
    au BufEnter,FocusGained,InsertLeave *.tex set concealcursor=nc
augroup END

nnoremap <localleader>vc <CMD>set concealcursor=nc<CR>

let g:vimtex_syntax_conceal = {
      \ 'accents': 1,
      \ 'ligatures': 1,
      \ 'cites': 1,
      \ 'fancy': 1,
      \ 'greek': 1,
      \ 'math_bounds': 1,
      \ 'math_delimiters': 1,
      \ 'math_fracs': 1,
      \ 'math_super_sub': 1,
      \ 'math_symbols': 1,
      \ 'sections': 1,
      \ 'styles': 1,
      \}

" yats
syntax keyword javascriptCommentTodoMine contained TODO FIXME XXX TBD HACK
syntax keyword typescriptCommentTodoMine contained TODO FIXME XXX TBD HACK

hi def link javascriptCommentTodoMine Todo
hi def link typescriptCommentTodoMine Todo

" Theme
set background=dark
set termguicolors
syntax on

" UltiSnips
set runtimepath+=~/.config/nvim/UltiSnips
let g:UltiSnipsSnippetStorageDirectoryForUltiSnipsEdit="~/.config/nvim/UltiSnips"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
let g:ultisnips_javascript = {
			\ 'keyword-spacing': 'always',
			\ 'semi': 'always',
			\ 'space_before_function_paren': 'never',
			\ }

" Vim Markdown
let g:markdown_fenced_languages = [
                                \ 'html',
                                \ 'css',
                                \ 'scss',
                                \ 'python',
                                \ 'py=python',
                                \ 'javascript',
                                \ 'js=javascript',
                                \ 'c',
                                \ 'lua',
                                \ 'ts=typescript',
                                \ ]

" ALE

" Disable ale lsp bkz we're using nvim lsp
let g:ale_disable_lsp = 1

let g:ale_linters = {
            \ 'javascript': ['prettier', 'eslint'],
            \ 'javascriptreact': ['prettier', 'eslint'],
            \ 'typescript': ['prettier', 'eslint'],
            \ 'typescriptreact': ['prettier', 'eslint'],
            \ 'python': ['flake8'],
            \ 'tex': ['chktex'],
            \ 'java': ['javac']
            \}
let g:ale_fixers = {
            \ 'json': ['trim_whitespace', 'prettier'],
            \ 'javascript': ['trim_whitespace', 'eslint', 'prettier'],
            \ 'javascriptreact': ['trim_whitespace', 'eslint', 'prettier'],
            \ 'typescriptreact': ['trim_whitespace', 'eslint', 'prettier'],
            \ 'typescript': ['trim_whitespace', 'eslint', 'prettier'],
            \ 'python': ['autopep8', 'trim_whitespace'],
            \ 'lua': ['trim_whitespace'],
            \ 'yaml': ['trim_whitespace', 'prettier'],
            \ 'java': ['trim_whitespace'],
            \ 'rust': ['trim_whitespace', 'rustfmt'],
            \}
let g:ale_pattern_options = {
            \ '\.min\.(js|css)$': {'ale_linters': [], 'ale_fixers': []},
            \}
let g:ale_python_flake8_use_global = 1
let g:ale_fix_on_save = 0

" LSP and Completion

set completeopt=menu,menuone

let g:completion_matching_strategy_list = ['exact', 'substring', 'fuzzy']

"===== NeoVim Configurations ====="

" rasi syntax highlighting
au BufNewFile,BufRead /*.rasi setf css

set wildmode=longest,list,full
set wildmenu
" Ignore files
set wildignore+=*.pyc
set wildignore+=*_build/*
set wildignore+=**/coverage/*
set wildignore+=**/node_modules/*
set wildignore+=**/android/*
set wildignore+=**/ios/*
set wildignore+=**/.git/*

let maplocalleader = ","

set title

" Don't need neovim showing me what mode I'm in twice
set noshowmode

" vertical splits on right by default
set splitright

" Fix dumb filetypes
augroup fixdumbfiletypes
    au!
    au BufNewFile,BufRead *.tex set filetype=tex
    au BufNewFile,BufRead *.cls set filetype=tex
augroup END

" Python Venvs
if has('unix')
    let g:python3_host_prog='~/.config/nvim/envs/neovim3/bin/python'
    let g:python_host_prog='~/.config/nvim/envs/neovim/bin/python'
    " Set node provider so nvim will always use lts-latest
    let g:node_host_prog='~/.fnm/aliases/lts-latest/bin/neovim-node-host'
else
    let g:python3_host_prog='~\.config\nvim\envs\neovim3\bin\python'
    let g:python_host_prog='~\.config\nvim\envs\neovim\bin\python'
endif

" Set encoding to utf-8
set encoding=UTF-8

" line wrapping
set linebreak
set wrap
set textwidth=100
set formatoptions-=t
" Don't split a word when wrapping text
set lbr

tnoremap <Esc><Esc> <c-\><c-n>

filetype plugin indent on


" Line Numbers
set number
set relativenumber

" auto set rnu on current split and absnum on others
augroup numbertoggle
    au!
    au BufEnter,FocusGained,InsertLeave * if &buftype ==# "terminal" || &buftype ==# "nofile" | echo "" | else | set rnu | endif
    au BufLeave,FocusLost,InsertEnter * if &buftype ==# "terminal" || &buftype ==# "nofile" | echo "" | else | set nornu | endif
augroup END

" Creates parent directories for new files if not exist.
function s:MkNonExDir(file, buf)
    if empty(getbufvar(a:buf, '&buftype')) && a:file!~#'\v^\w+\:\/'
        let dir=fnamemodify(a:file, ':h')
        if !isdirectory(dir)
            call mkdir(dir, 'p')
        endif
    endif
endfunction
augroup BWCCreateDir
    autocmd!
    autocmd BufWritePre * :call s:MkNonExDir(expand('<afile>'), +expand('<abuf>'))
augroup END

" Make vim confirm if I forget to save
set confirm

" Setup highlight for search
set incsearch
" Only highlight the first match
set nohlsearch

" Show @@@ in the last line if it's trucated
set display=truncate

" CD to the containing directory of the current file
map <leader>cd :cd %:p:h<CR>:pwd<CR>

" Show partial commands
set showcmd

" Backup files and send all mess files to the same place
set backup
set backupdir=~/.config/nvim/.neovim_backups
set dir=~/.config/nvim/.neovim_tmp

" Persistent undo
set undofile
set undodir=~/.config/nvim/.nvim_undo
set undolevels=1000
set undoreload=10000

" Add more matchedpairs
" TODO: Check if this actually works/is there a better way?
set matchpairs+=<:>
set matchpairs+=":"
set matchpairs+=':'
set matchpairs+=`:`

" Color column to visibly show 80 col for reference
set colorcolumn=80

" Indents
set shiftwidth=4
set tabstop=4
set copyindent
set smarttab
set expandtab
augroup SpecificFileTypeIndents
	au!
	au FileType typescript setlocal tabstop=2 shiftwidth=2
	au FileType javascript setlocal tabstop=2 shiftwidth=2
	au FileType typescriptreact setlocal tabstop=2 shiftwidth=2
	au FileType javascriptreact setlocal tabstop=2 shiftwidth=2
	au FileType json setlocal tabstop=2 shiftwidth=2
	au FileType yaml setlocal tabstop=2 shiftwidth=2
	au FileType html setlocal tabstop=2 shiftwidth=2
	au FileType css setlocal tabstop=2 shiftwidth=2
	au FileType scss setlocal tabstop=2 shiftwidth=2
augroup END

" Folding
set foldenable
set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()
set foldlevel=99999

if (has("termguicolors"))
  set termguicolors
endif

"===== Keybinds ====="

" print and yank from clipboard
nnoremap <leader>p "*p
nnoremap <leader>y "*y

" New line then clear (for when comments are auto added n shit)
nnoremap <leader>o o<Esc>0"_D
nnoremap <leader>O O<Esc>0"_D

nnoremap <leader>+ :vertical resize +5<CR>
nnoremap <leader>- :vertical resize -5<CR>

" Inkscape figures
"inoremap <C-f> <Esc>: silent exec '.!inkscape-figures create "'.getline('.').'" "'.b:vimtex.root.'/figures/"'<CR><CR>:w<CR>
"nnoremap <C-f> : silent exec '!inkscape-figures edit "'.b:vimtex.root.'/figures/" > /dev/null 2>&1 &'<CR><CR>:redraw!<CR>

" Git diff
nmap <leader>gj :diffget //3<CR>
nmap <leader>gf :diffget //2<CR>

" Toggle cursorline
nnoremap <leader>tcl :ToggleCursorLine<CR>
command! ToggleCursorLine set cursorline!

" Delete line (keeping blank line)
nnoremap dl 0D

" get me $MYVIMRC faster
cnoremap <leader>vrc $MYVIMRC
cnoremap <leader>lua $MYVIMLUA

" Better tab commands
nnoremap th :tabfirst<CR>
nnoremap tk :tabnext<CR>
nnoremap tj :tabprev<CR>
nnoremap tl :tablast<CR>
nnoremap te :tabedit<Space>
nnoremap tn :tabnew<CR>
nnoremap td :tabclose<CR>
nnoremap tm :tabmove<Space>

" Tab nav using leader and num row
nnoremap <leader>1 1gt
nnoremap <leader>2 2gt
nnoremap <leader>3 3gt
nnoremap <leader>4 4gt
nnoremap <leader>5 5gt
nnoremap <leader>6 6gt
nnoremap <leader>7 7gt
nnoremap <leader>8 8gt
nnoremap <leader>9 9gt

" Ale fix
nmap <leader>l :ALEFix<CR>

" Redo last editor command
nmap <leader>. @:

" Surround
nmap <silent> s <Plug>Ysurround
vmap <silent> s S

" FZF TODO: DELETE
"nnoremap <leader>f :Files<CR>
"nnoremap <leader>fg :GFiles<CR>
"nnoremap <leader>fgs :GFiles?<CR>
"nnoremap <leader>b :Buffers<CR>
"nnoremap <leader>snip :Snippets<CR>
"nnoremap <leader>co :Commits<CR>
"nnoremap <leader><leader> :Rg<CR>

nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fc <cmd>Telescope trnt dotfiles<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>fh <cmd>Telescope help_tags<cr>

"imap <c-x><c-k> <plug>(fzf-complete-word)
"inoremap <expr> <c-x><c-f> fzf#vim#complete#path($FZF_DEFAULT_COMMAND)
"imap <c-x><c-l> <plug>(fzf-complete-line)

nnoremap <leader>m :MaximizerToggle!<CR>

" Nvim Tree
nnoremap <leader>n  <cmd>NvimTreeToggle<CR>
nnoremap <leader>r  <cmd>NvimTreeRefresh<CR>
nnoremap <leader>ft <cmd>NvimTreeFindFile<CR>

" let j and k work well with line wrapping
nmap j gj
nmap k gk

" Yank to end of line
noremap Y y$

" better split navigation
nnoremap <A-S-H> <C-W><C-H>
nnoremap <A-S-J> <C-W><C-J>
nnoremap <A-S-K> <C-W><C-K>
nnoremap <A-S-L> <C-W><C-L>

" up and down 5 line at a time
nmap <C-j> 5j
vmap <C-j> 5j
nmap <C-k> 5k
vmap <C-k> 5k

" Moving lines
nnoremap <A-j> :m .+1<CR>==
nnoremap <A-k> :m .-2<CR>==
inoremap <A-j> <Esc>:m .+1<CR>==gi
inoremap <A-k> <Esc>:m .-2<CR>==gi
vnoremap <A-j> :m '>+1<CR>gv=gv
vnoremap <A-k> :m '<-2<CR>gv=gv

" mappings for s, %s, / and, ?
nnoremap / /\v
vnoremap / /\v
nnoremap ? ?\v
vnoremap ? ?\v
nmap <space> /
vmap <space> /
nmap <BS> ?
vmap <BS> ?

nmap <leader>ss :smagic/
nmap <leader>ms :%smagic/
nnoremap <leader>is :%s/\<<C-r><C-w>\>/

" Window (not-cursor) scrolling
nnoremap <Right> ;
nnoremap <Left> ,
nnoremap <Up> <c-y>
nnoremap <Down> <c-e>

" Toggle Spelling
nnoremap <leader>s :ToggleSpelling<CR>
command! ToggleSpelling silent set spell! spelllang=en
inoremap <C-l> <c-g>u<esc>[s1z=`]a<c-g>u

if !has('unix')
    " Open the current file's containing directory in explorer
    nnoremap <leader>ef !start %:p:h<CR>
    " Open the PWD in explorer
    nnoremap <leader>e !start .<CR>
endif

" Open the current file's matching pdf file
map <F8> :PDF<CR>
if has('unix')
    command! PDF silent execute "!zathura %:r.pdf &"
else
    command! PDF silent execute "!start /b sumatrapdf %:r.pdf"
endif

" Convert current file to pdf through latex using Pandoc
map <F7> :PandocPDF<CR>
if has('unix')
    command! PandocPDF silent execute "!pandoc -dnotes --quiet % -o %:r.pdf &"
else
    command! PandocPDF silent execute "!start /b pandoc -dnotes --quiet % -o %:r.pdf"
endif

"===== Miscellaneous ====="

" Switch between markdown and pandoc fast
nnoremap <leader>mp :SwitchTypeMarkdownPandoc<CR>
command! SwitchTypeMarkdownPandoc silent if &filetype ==# "markdown" | setlocal filetype=markdown.pandoc | elseif &filetype ==# "markdown.pandoc" | setlocal filetype=markdown | endif

" FIXME: Make this more better cause I keep on accidentally making pdfs
" Auto Pandoc Compilation
augroup AutoPandocCompilation
	au!
	" Pandoc Files should by default be markdown.pandoc files
	au FileType pandoc setlocal filetype=markdown.pandoc
	" README files should be markdown, not pandoc
	au BufNewFile,BufRead README.md setlocal filetype=markdown
	au BufNewFile,BufRead CHANGELOG.md setlocal filetype=markdown
	au BufNewFile,BufRead CONTRIBUTING.md setlocal filetype=markdown
	au BufNewFile,BufRead LICENSE.md setlocal filetype=markdown
	au BufNewFile,BufRead README setlocal filetype=markdown

	" Pandoc files should be automatically compiled to pdf
    au BufWritePost *.md if &filetype ==# "markdown.pandoc" | silent PandocPDF

    " All markdown files should use pandoc syntax
    au FileType markdown setlocal syntax=markdown.pandoc
augroup END

" Set the highlighting for Conceals
highlight Conceal guifg=darkcyan ctermfg=darkcyan guibg=NONE ctermbg=NONE

" FIXME: Some of these don't work as expected (either me setup wrong or use wrong cmd).
nnoremap <leader>vd :lua vim.lsp.buf.definition()<CR>
nnoremap <leader>vi :lua vim.lsp.buf.implementation()<CR>
nnoremap <leader>vsh :lua vim.lsp.buf.signature_help()<CR>
nnoremap <leader>vrr :lua vim.lsp.buf.references()<CR>
nnoremap <leader>vrn :lua vim.lsp.buf.rename()<CR>
nnoremap <leader>vh :lua vim.lsp.buf.hover()<CR>
nnoremap <leader>vca :lua vim.lsp.buf.code_action()<CR>
nnoremap <leader>vsd :lua vim.diagnostic.open_float({scope="line"})<CR>
nnoremap <leader>vn :lua vim.lsp.diagnostic.goto_next()<CR>
nnoremap <leader>vp :lua vim.lsp.diagnostic.goto_prev()<CR>

" Replace spaces with tabs
command! Sp2Tabs silent setlocal tabstop=2 | setlocal noexpandtab | %retab!

" TODO: move this somewhere nice and maybe change it a bit
" background trans
hi Normal guibg=none ctermbg=none
hi NonText guibg=none ctermbg=none
" Makes line nums easier to read by making brighter
highlight LineNr term=bold cterm=NONE ctermfg=DarkGrey ctermbg=NONE gui=NONE guifg=DarkGrey guibg=NONE

" Don't put anything after this "
if has('win32')
    let WebDevIconsOS='win32'
else
    let WebDevIconsOS='Debian'
endif
if exists("g:loaded_webdevicons")
    call webdevicons#refresh()
endif
