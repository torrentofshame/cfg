#!/bin/bash

PLUGGED_DIR="$HOME/.config/nvim/plugged"
PATCH_DIR="$HOME/.config/nvim/plugged-patches"

git apply --directory="$PLUGGED_DIR/$1" "$PATCH_DIR/$1.patch"
