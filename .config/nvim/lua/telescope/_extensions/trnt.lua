local builtins = require("telescope.builtin")

--[[ Custom Extension for custom functions ]]

local e = {}

function e.dotfiles ()
    -- Searches through the dotfiles according to .cfg bare repo index
    local git_command = {
        "/usr/bin/git",
        "--git-dir="..vim.env.HOME.."/.cfg/",
        "--work-tree="..vim.env.HOME.."",
        "ls-files",
        "--exclude-standard",
        "--cached",
        "--recurse-submodules"
    }

    builtins.find_files{
        prompt_title = "Dotfiles",
        find_command = git_command,
        hidden = true,
        cwd = vim.env.HOME
    }

end

return require("telescope").register_extension({ exports = e })
