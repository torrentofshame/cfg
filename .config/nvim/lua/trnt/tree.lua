--require'nvim-tree'.setup{}

--require('ts_context_commentstring').setup{}

--vim.g.skip_ts_context_commentstring_module = true

require'nvim-treesitter.configs'.setup{
    highlight = {
        ensure_installed = {
            "c", "lua", "rust", "bash", "css", "dockerfile", "go", "html",
            "java", "javascript", "jsdoc", "json", "latex", "make", "python",
            "regex", "scss", "tsx", "svelte", "vim", "yaml", "toml", "typescript"
        },
        enable = true,
        additional_vim_regex_highlighting = false
    },
    incremental_selection = {
        enable = true,
        keymaps = {
            init_selection = "gnn",
            node_incremental = "grn",
            scope_incremental = "grc",
            node_decremental = "grm",
        }
    },
    indent = {
        enable = true
    }
}
