local pickers = require("telescope.pickers")
local finders = require("telescope.finders")
local previewers = require("telescope.previewers")
local action_state = require("telescope.actions.state")
local conf = require("telescope.config").values
local actions = require("telescope.actions")


require("telescope").setup{
    defaults = {
        file_sorter = require("telescope.sorters").get_fzy_sorter,
        color_devicons = true,

        file_previewer = require("telescope.previewers").vim_buffer_cat.new,
        grep_previewer = require("telescope.previewers").vim_buffer_vimgrep.new,
        qflist_previewer = require("telescope.previewers").vim_buffer_qflist.new
    },
    extensions = {
        ["ui-select"] = {
            require("telescope.themes").get_dropdown{}
        }
    }
}

require("telescope").load_extension("ui-select")
require("telescope").load_extension("fzy_native")
--require("telescope").load_extension("ultisnips")
--require("telescope").load_extension("media_files")
require("telescope").load_extension("trnt")
