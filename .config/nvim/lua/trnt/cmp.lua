--[[ Setup nvim-cmp ]]
local cmp = require"cmp"

local lspkind = require"lspkind"

cmp.setup{
--[[    snippet = {
        expand = function(args)
            vim.fn["UltiSnips#Anon"](args.body)
        end
    },--]]
    mapping = {
        ['<c-p>'] = function(fallback)
            if cmp.visible() then
                cmp.select_prev_item()
            else
                fallback()
            end
        end,
        ['<c-n>'] = function(fallback)
            if cmp.visible() then
                cmp.select_next_item()
            else
                fallback()
            end
        end,
        ["<C-d>"] = cmp.mapping.scroll_docs(-4),
        ["<C-f>"] = cmp.mapping.scroll_docs(4),
        ["<C-Space>"] = cmp.mapping.complete(),
        ["<C-e>"] = cmp.mapping.abort(),
        ["<TAB>"] = cmp.mapping.confirm({ select = true })
    },
    sources = cmp.config.sources{
        { name = "nvim_lsp" },
        { name = "ultisnips" },
        { name = "buffer", max_item_count = 10 },
        { name = 'nvim_lsp_signature_help' },
    },
    completion = {
        completeopt = "menuone"
    },
    formatting = {
        format = lspkind.cmp_format{with_text = true, maxwidth = 50, menu = {
                buffer = "[Bufer]",
                nvim_lsp = "[LSP]",
                ultisnips = "[UltiSnips]",
                nvim_lua = "[Lua]",
                latex_symbols = "[Latex]",
                path = "[Path]"
        }}
    }
}

cmp.setup.filetype('gitcommit', {
    sources = {
        {
            name = 'commit'
        }
    }
})
require('cmp_commit').setup{
    block = { "__pycache__", "CMakeFiled", "node_modules", "target" },
}

cmp.setup.cmdline('/', {
    sources = cmp.config.sources({
        { name = 'nvim_lsp_document_symbol' }
    }, {
        { name = 'buffer' }
    })
})


