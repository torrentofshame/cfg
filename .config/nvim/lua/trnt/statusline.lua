local quantum = require("lualine.themes.quantum");

require("lualine").setup{
    options = {
        theme = quantum
    },
    extensions = {
        "fzf",
        "nvim-tree",
        "fugitive",
        "symbols-outline"
    }
}
