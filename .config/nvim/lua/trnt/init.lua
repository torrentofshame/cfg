vim.loader.enable()
require("trnt.lazy")

P = function(v)
    print(vim.inspect(v))
    return v
end

require('Comment').setup{
    pre_hook = require('ts_context_commentstring.integrations.comment_nvim').create_pre_hook()
}
