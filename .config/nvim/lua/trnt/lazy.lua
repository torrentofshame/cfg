local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
    vim.fn.system({
        "git",
        "clone",
        "--filter=blob:none",
        "https://github.com/folke/lazy.nvim.git",
        "--branch=stable", -- latest stable release
        lazypath
    })
end
vim.opt.rtp:prepend(lazypath)

vim.g.mapleader = "\\"

require("lazy").setup{
    -- Theme
    {
        "torrentofshame/nvim-quantum",
        priority = 1000,
        lazy = false,
        config = function()
            require("quantum").setup{}
        end
    },
    -- StatusLine
    {
        "nvim-lualine/lualine.nvim",
        event = "VeryLazy",
        config = function()
            require("trnt.statusline")
        end
    },
    {
        "nvim-lua/popup.nvim",
        dependencies = {
            "nvim-lua/plenary.nvim",
        }
    },
    -- Telescope
    {
        "nvim-telescope/telescope.nvim",
        tag = "0.1.4",
        dependencies = {
            "nvim-lua/plenary.nvim",
            "nvim-telescope/telescope-fzy-native.nvim",
            "nvim-telescope/telescope-ui-select.nvim",
        },
        config = function()
            require("trnt.telescope")
        end
    },
    --
    {
        "nvim-treesitter/nvim-treesitter",
        build = ":TSUpdate",
        event = "VeryLazy",
        dependencies = {
            "nvim-treesitter/playground",
            "nvim-treesitter/nvim-treesitter-context",
            "JoosepAlviste/nvim-ts-context-commentstring",
        },
        config = function()
            require("trnt.tree")
        end
    },
    {"numToStr/Comment.nvim", event="VeryLazy"},
    --
    {dir = "~/.config/nvim/local-plugins/discord-rpc.nvim", lazy=false},
    "Yggdroot/indentLine",
    {"godlygeek/tabular", lazy=false},
    {"tpope/vim-markdown", ft = {"markdown"}}, -- maybe not needed
    -- LaTeX
    {
        "vim-pandoc/vim-pandoc",
        ft = {
            "markdown",
            "pandoc.markdown"
        },
        dependencies = {
            "vim-pandoc/vim-pandoc-syntax",
        }
    },
    {"lervag/vimtex", build = "~/.config/nvim/plug-patcher.sh vimtex &>/dev/null"}, -- TODO: Defunct?
    -- Nvim LSP
    {
        "neovim/nvim-lspconfig",
        event = { "BufReadPost", "BufNewFile" },
        cmd = { "LspInfo", "LspInstall", "LspUninstall" },
        dependencies = {
            "nvim-lua/lsp_extensions.nvim",
            "simrat39/rust-tools.nvim",
        },
        config = function()
            require("trnt.lsp")
        end
    },
    {
        "simrat39/symbols-outline.nvim",
        config = function()
            require("symbols-outline").setup{
                highlight_hovered_item = true,
                show_guides = true,
            }
        end
    },
    --[[
    {
        "SirVer/ultisnips",
        event = "InsertEnter",
        dependencies = {
            "honza/vim-snippets"
        }
    },
    --]]
    {
        "hrsh7th/nvim-cmp",
        event = "VeryLazy",
        dependencies = {
            "kdheepak/cmp-latex-symbols",
            "hrsh7th/cmp-nvim-lsp",
            "hrsh7th/cmp-buffer",
            "hrsh7th/cmp-path",
            "hrsh7th/cmp-cmdline",
            "hrsh7th/cmp-nvim-lsp-document-symbol",
            "hrsh7th/cmp-nvim-lsp-signature-help",
            "Dosx001/cmp-commit",
            "onsails/lspkind-nvim",
--            "SirVer/ultisnips",
        },
        config = function()
            require("trnt.cmp")
        end
    },
    "gpanders/nvim-parinfer",
	-- Trouble.nvim
    {
        "nvim-tree/nvim-tree.lua",
        version = "*",
        lazy = false,
        dependencies = {
            "nvim-tree/nvim-web-devicons",
        },
        config = function()
            require("nvim-tree").setup{}
        end,
    },
	-- Misc
	"tpope/vim-surround", -- TODO: see if replacement?
    {"wakatime/vim-wakatime", lazy=false},
	"dense-analysis/ale", -- TODO: replace this with something better/newer
	"tpope/vim-characterize", --
	"mhinz/vim-signify",
	"tpope/vim-fugitive",

    { "tweekmonster/startuptime.vim",
        cmd = "StartupTime"
    },

    {"ryanoasis/vim-devicons", priority=0, lazy=false}
}
