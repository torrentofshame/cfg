local cfgs = require"lspconfig/configs"

--[[ Setup Language servers ]]
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

local LspCfg = {}
setmetatable(LspCfg, {
    __index = function(table, key)
        local tbl = require("lspconfig")[key]
        local oldsetup = tbl.setup

        tbl.setup = function(args)
            args.capabilities = capabilities
            return oldsetup(args)
        end

        return tbl
    end
})

LspCfg.pyright.setup{}

LspCfg.grammarly.setup{}

LspCfg.tsserver.setup{}

--LspCfg.ccls.setup{}

LspCfg.arduino_language_server.setup{}

--[[
LspCfg.clangd.setup{
    root_dir = function() return vim.loop.cwd() end
}
--]]

LspCfg.svelte.setup{}

LspCfg.yamlls.setup{}

LspCfg.jdtls.setup{
    cmd = { "/usr/lib/jvm/openjdk11/bin/java", "-Declipse.application=org.eclipse.jdt.ls.core.id1", "-Dosgi.bundles.defaultStartLevel=4", "-Declipse.product=org.eclipse.jdt.ls.core.product", "-Dlog.protocol=true", "-Dlog.level=ALL", "-Xms1g", "-Xmx2G", "--add-modules=ALL-SYSTEM", "--add-opens", "java.base/java.util=ALL-UNNAMED", "--add-opens", "java.base/java.lang=ALL-UNNAMED", "-jar", "/home/torrentofshame/.local/opt/jdt/plugins/org.eclipse.equinox.launcher_1.6.400.v20210924-0641.jar", "-configuration", "/home/torrentofshame/.local/opt/jdt/config_linux", "-data", "/home/torrentofshame/workspace" },
    root_dir = function() return vim.loop.cwd() end
}

LspCfg.ansiblels.setup{}

LspCfg.html.setup{}

require('rust-tools').setup{}

--[[
LspCfg.rust_analyzer.setup{
    assist = {
        importGranularity = "module",
        importPrefix = "self"
    },
    cargo = {
        loadOutDirsFromCheck = true
    },
    procMacro = {
        enable = true
    }
}
--]]

--LspCfg.denols.setup{}

LspCfg.cssls.setup{}

LspCfg.cssmodules_ls.setup{}

local runtime_path = vim.split(package.path, ';')
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?/init.lua")

local libraries = vim.api.nvim_get_runtime_file("", true)

local awesomeHome = os.getenv("HOME") .. "/.config/awesome"

local custom_library_paths = {
    "/usr/share/awesome/lib",
    awesomeHome
}

for _,v in ipairs(custom_library_paths) do
    libraries[#libraries+1] = v
end

LspCfg.lua_ls.setup{
    settings = {
        Lua = {
            runtime = {
                path = runtime_path
            },
            diagnostics = {
                globals = {
                    "vim",
                    "screen",
                    "mouse",
                    "awesome",
                    "client",
                    "tag",
                    "type",
                    "root"
                }
            },
            workspace = {
                library = libraries
            },
        }
    }
}

LspCfg.terraformls.setup{}

LspCfg.hls.setup{}

LspCfg.csharp_ls.setup{}

LspCfg.gopls.setup{}

LspCfg.htmx.setup{}
