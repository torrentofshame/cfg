#!/bin/bash

OUTPUT_DIR=/tmp/conky

if ! [ -d $OUTPUT_DIR ]; then
    mkdir -p $OUTPUT_DIR
fi

zip="32826"

# $1: After Sunset? - bool
get_today_heb () {
    url="https://www.hebcal.com/converter?cfg=json&date=$(date +%Y-%m-%d)&g2h=1&strict=1&lg=sh"
    if $1; then
        url="$url&gs=on"
    fi
    curl -s "$url" 
}

zmanim=`curl -s "https://www.hebcal.com/zmanim?cfg=json&geo=zip&zip=${zip}&date=$(date +%Y-%m-%d)"`

# $1: Name of zmanim in zmanim api output - String
get_ztime () {
    date -d `echo $zmanim | jq -r ".times.${1}"` +"%-I:%M %p"
}

# Returns the time 18 minutes before sunset
get_candletime () {
    date -d "$(date -d `echo $zmanim | jq -r ".times.sunset"`) - 18 minutes" +"%-I:%M %p"
}


cat << EOF > $OUTPUT_DIR/zmanim_formatted
\${color6}\${font Roboto:size=6}Alot Hashachar\${alignr}$(get_ztime "alotHaShachar")
Misheyakir\${alignr}$(get_ztime "misheyakirMachmir")
Sunrise\${alignr}$(get_ztime "sunrise")
Midday\${alignr}$(get_ztime "chatzot")
Mincha Gedolah\${alignr}$(get_ztime "minchaGedola")
Mincha Ketanah\${alignr}$(get_ztime "minchaKetana")
Plag Hamincha\${alignr}$(get_ztime "plagHaMincha")
\${if_match "\${time %A}" == "Friday"}\
Candle Lighting\${alignr}$(get_candletime)
\${endif}\
Sunset\${alignr}$(get_ztime "sunset")
Nightfall\${alignr}$(get_ztime "dusk")

EOF

sunset=`date -d "$(echo $zmanim | jq -r '.times.sunset')" +%s`
now=`date +%s`

# If before sunset
if [ $sunset -gt $now ]; then
    today=`get_today_heb false`
    # Schedule to re-run itself 1 min after sunset
    echo "$HOME/.local/bin/hebrew_calendar" | at -M `date -d"$(date -d "@$sunset") 1 minute" +%I:%M%p`
else # If after (or at) sunset
    today=`get_today_heb true`
fi

heb_date_en=`echo $today | jq -r '[.hd, .hm, .hy] | map(tostring) | join(" ")' | sed -E 's/(\s[0-9]{4})/,\1/'`
heb_date=`echo $today | jq -r '.heDateParts | .d + " " + .m + " " + .y' | rev`

echo $heb_date_en > $OUTPUT_DIR/hebdate_en
echo $heb_date > $OUTPUT_DIR/hebdate
echo $zmanim | jq '.times' > $OUTPUT_DIR/zmanim
