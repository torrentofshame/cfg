if command -v exa --help &>/dev/null; then
    alias ls='exa --group-directories-first'
    alias l='ls -F'
    alias lt='ls -S --classify'
    alias lg='ls -lg'
else
    alias ls='ls --color=auto --group-directories-first'
    alias l='ls -CF'
    alias lt='ls --human-readable --size -1 -S --classify'
fi
alias ll='ls -l'
alias la='ls -a'
alias sl='ls' # I can't type lmao

alias psgrep='ps aux | head -n 1; ps aux | grep'

alias src='source'
pysrc() {
	[ -f "${1:-venv}/bin/activate" ] && src "${1:-venv}/bin/activate" || echo >&2 -e "pysrc: No python virtual environment detected at ${1:-venv}"
}

alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'

alias rdir='export PS1DIR=0'
alias adir='export PS1DIR=1'

ci() {
    __zoxide_zi "$@"
}

alias loadbrc='source ~/.bashrc'
alias cdtop='cd ~/Desktop'
alias dtop='cd ~/Desktop'
alias down='cd ~/Downloads'
alias docs='cd ~/Documents'
alias editbashrc='nvim ~/.bashrc'
alias editalias='nvim ~/.bash_aliases'

alias mkdir='mkdir -p'

alias ve='python3 -m venv ./venv'
alias va='source ./venv/bin/activate'

case "$DISTRO" in
    debian)
        alias gapt='sudo apt list|grep'
        alias gapti='sudo apt list --installed|grep'
        alias install='sudo apt-get install'
        alias remove='sudo apt-get remove'
        alias purge='sudo apt-get purge'
        alias update='sudo apt-get update'
        alias clean='sudo apt-get autoclean && sudo apt-get autoremove'
        alias apt-get='sudo apt-get'
    ;;
    void)
        alias install='sudo xbps-install -S'
        alias query='xbps-query -Rs'
    ;;
    *) ;;
esac

alias cdtmp='cd $(mktemp -d -t ci-XXXXXXXXX)'
alias repos='cd ~/code-repos'
alias cd..='cd ..'
alias ..='cd ..'
alias ...='cd ../..'
alias ....='cd ../../..'
alias .....='cd ../../../..'

# FIXME: cd command not using output of my bullshit!
cds_nuts() {
    cd ".$(echo "$@" | sed 's@/@\n/@' | sed -e '/^\.*$/ {s/\.\././; s/\./\/../g}' | tr -d '\n')"
}
#alias cd='cds_nuts'

alias c='clear'
alias x="exit"

alias vi='vim'
alias svi='sudo vi'
alias edit='vim'

alias fzr='__fzf_code_repos__'

alias nvd='nvim -c Gvdiffsplit!'
alias cnvd='GIT_DIR=$HOME/.cfg/ GIT_WORK_TREE=$HOME nvd'

alias unsshfs='fusermount -u'

alias xclip='xclip -selection c'

alias g='git'
alias dox='doxygen'
alias doxg='doxygen -g'

alias zathdef='zathura -c /dev/null'

alias ssh='TERM=xterm-256color ssh'

alias pn='pnpm'
alias npmlts='nvm exec lts/* npm'

# To force things to only use p-cores
alias pcoresonly='taskset -c 0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15'

alias startloopback='pactl load-module module-loopback latency_msec=1'
alias stoploopback='pactl unload-module module-loopback'

# Stuff so I can use a bare git repo to sync my config files
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'

if command -v fnm --help &>/dev/null; then
    alias nvm='fnm'
fi

if command -v bat --help &>/dev/null; then
    alias cat='bat'
fi

alias rnver="cat package.json | grep \\"react-native\\": | sed 's/[\",]//g' | awk '{ print $2 }'"

nexec() {
    nvim --cmd "!touch "$@"" -c "!chmod u+x "$@"" -- "$@"
}
