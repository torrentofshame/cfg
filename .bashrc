# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# don't put duplicate lines or lines starting with space in the history.
# See bash(1) for more options
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)
HISTSIZE=1000
HISTFILESIZE=2000

DISTRO=$(sed -n 's/^ID=\W\?\(\w\+\)\W\?$/\1/p' /etc/os-release)

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color|*-256color) color_prompt=yes;;
esac

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi
unset color_prompt force_color_prompt

# If this is an xterm set the title to user@host:dir
#case "$TERM" in
#xterm*|rxvt*)
#    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
#    ;;
#*)
#    ;;
#esac

# Add .local bin
if [ -d "$HOME/.local/bin" ]; then
    export PATH="$HOME/.local/bin:$PATH"
fi

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

if [ -d ~/.bashrc.d/ ]; then
	for f in `fd . -t f ~/.bashrc.d`; do
		[ -r "$f" ] && . "$f"
	done
	unset f
fi

if [ -f /usr/bin/nvm ]; then
    export NVM_DIR="$HOME/.nvm"
    [ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
    [ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
fi

# Add texlive to path
export PATH="/opt/texlive/2023/bin/x86_64-linux:${PATH}"

# Autocompletion for terraform
if command -v terraform -help &> /dev/null; then
    #terraform -install-autocomplete
    complete -C /usr/bin/terraform terraform
fi

# Script to extract archives

extract() {
    if [ -z ${1} ] || [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
        echo "Usage: extract <archive> [directory]"
        echo "Example: extract presentation.zip."
        echo "Valid archive types are:"
        echo "tar.bz2, tar.gz, tar.xz, tar, bz2, gz, tbz2,"
        echo "tbz, tgz, lzo, rar, zip, 7z, xz, txz, lzma and tlz"
    else
        case "$1" in
            *.tar.bz2|*.tbz2|*.tbz)         tar xvjf "$1" ;;
            *.tgz)                          tar zxvf "$1" ;;
            *.tar.gz)                       tar xvzf "$1" ;;
            *.tar.xz)                       tar xvJf "$1" ;;
            *.tar)                          tar xvf "$1" ;;
            *.rar)                          7z x "$1" ;;
            *.zip)                          unzip "$1" ;;
            *.7z)                           7z x "$1" ;;
            *.lzo)                          lzop -d  "$1" ;;
            *.gz)                           gunzip "$1" ;;
            *.bz2)                          bunzip2 "$1" ;;
            *.Z)                            uncompress "$1" ;;
            *.xz|*.txz|*.lzma|*.tlz)        xz -d "$1" ;;
            *) echo "Sorry, '$1' could not be decompressed." ;;
        esac
    fi
}

if command -v thefuck &> /dev/null; then
eval "$(thefuck --alias)"
fi

# FZF
case "$DISTRO" in
    void)
        if [ -d /usr/share/fzf/ ]; then
            source /usr/share/fzf/key-bindings.bash
        fi
        #if [ -d $HOME/.bashrc.d/fzf/ ]; then
            #source $HOME/.bashrc.d/fzf/key-bindings.bash
            #source $HOME/.bashrc.d/key-bindings.bash
        #fi
    ;;
    *)
        if [ -d /usr/share/doc/fzf/ ]; then
            source /usr/share/doc/fzf/examples/key-bindings.bash
        fi
    ;;
esac

# Fast p stuff (fzf through pdfs)
p () {
    open=xdg-open   # this will open pdf file withthe default PDF viewer on KDE, xfce, LXDE and perhaps on other desktops.

    fd -t f -e pdf -E 'done_*' \
        | fast-p \
        | fzf --read0 --reverse -e -d $'\t'  \
        --preview-window down:80% --preview '
            v=$(echo {q} | tr " " "|"); 
            echo -e {1}"\n"{2} | grep -E "^|$v" -i --color=always;
            ' \
                | cut -z -f 1 -d $'\t' | tr -d '\n' | xargs -r --null $open > /dev/null 2> /dev/null
            }

export FZF_COMPLETION_OPTS='--info=inline'

# restart pulseaudio and bluetooth
restartbluetooth() {
    pulseaudio -k
    start-pulseaudio-x11
    sudo service bluetooth restart
}

NAME=$(hostname)

# For gpg-agent ssh yubikey stuff
export GPG_TTY="$(tty)"
export SSH_AUTH_SOCK=$(gpgconf --list-dirs agent-ssh-socket)
export GNUPGHOME="~/.gnupg"

# TODO: perhaps replace this shit w/ a user service?
gpgconf -K gpg-agent > /dev/null
gpgconf --launch gpg-agent > /dev/null

# Customize the prompt
_git_stats() {
    branch_char=$'\ue0a0'
    branch=$(git rev-parse --abbrev-ref HEAD 2>/dev/null)
    if [[ -n "$branch" ]]
    then
        bare=$(git rev-parse --is-bare-repository)
    if [ $bare == 'true' ]
    then
        branch="~bare"
    fi
	status=$(git status 2> /dev/null)
	if [[ $status =~ 'Untracked files'  ]]
       	then
	    mod="?"
        fi
	if [[ $status =~ 'Changes to be committed'  ]]
       	then
	    mod="+"
        fi
	if [[ $status =~ 'Your branch is ahead'  ]]
       	then
	    mod=">"
        fi
	if [[ $status =~ 'Your branch is behind'  ]]
       	then
	    mod="<"
        fi
        echo -n "\[\e[94m\]git:(\[\e[91m\]${branch}\[\e[35m\]${mod}\[\e[94m\])"
    fi
    return 0
}

# Find out if inside Python virtualenv
_pyvenv_stats() {
    if test -z "$VIRTUAL_ENV"
    then
        echo -n ""
    else
        echo -n "\[\e[94m\](\[\e[91m\]`basename \"$VIRTUAL_ENV\"`\[\e[94m\])\[\e[0m\]"
    fi
}

_make_prompt() {
    local git_st
    if [[ $(id -u) -ne 0 ]]
    then
        ISROOT=""
    else
        #ISROOT="\[\e[91m\]root\[\e[00m\]"
        ISROOT="\[\e[91m\]"
    fi
    if [ -n "$SSH_CLIENT" ] || [ -n "$SSH_TTY" ]
    then
        ISSSH=$'\uE268 '
    else
        ISSSH=""
    fi
    if [[ $PS1DIR -eq 0 ]]
    then
        PS1="${ISROOT}${ISSSH}-> \[\e[32m\]\W"
    else
        PS1="${ISROOT}${ISSSH}-> \[\e[32m\]\w"
    fi
    git_st=$(_git_stats)
    if [[ -n "$git_st" ]]
    then
        git_st=" ${git_st}"
    fi
    PS1="$(_pyvenv_stats)${PS1}${git_st}\[\e[00m\] "
    return 0
}

PROMPT_COMMAND="_make_prompt;"
if [ -d "$HOME/.cargo" ]; then
    . "$HOME/.cargo/env"
fi

if command -v kubectl &> /dev/null; then
    source <(kubectl completion bash)
fi

if command -v yq-go &> /dev/null; then
    source <(yq-go shell-completion bash)
    alias yq='yq-go'
fi

export ANDROID_HOME=/opt/android_sdk
export ANDROID_SDK_ROOT=/opt/android_sdk

export PATH=$PATH:$ANDROID_HOME/cmdline-tools/latest/bin
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools

export JDTLS_HOME=/opt/jdt
export JAVA_HOME=/usr/lib/jvm/openjdk17/

export JUNIT_HOME=$HOME/.local/opt/junit

export CLASSPATH=$CLASSPATH:$JUNIT_HOME/junit.jar
export CLASSPATH=$CLASSPATH:$JUNIT_HOME/
 
export PATH=$PATH:/usr/lib/ruby/gems/3.1.0/bin
export PATH=$PATH:/usr/lib/ruby/gems/3.1.0

export PATH=$PATH:$HOME/go/bin

#export JAVA_OPTS="$JAVA_OPTS -Xms1024m -Xmx2048m"

# fnm
export PATH=/home/torrentofshame/.fnm:$PATH
eval "`fnm env`"
eval "$(fnm env --use-on-cd)"

alias luamake=/home/torrentofshame/.local/src/lua-language-server/3rd/luamake/luamake

# Lists items in dir1 that are not in dir2
diffdir() {
	ls $1 | grep --color=always -v `ls -1 $2 | awk 'BEGIN {ORS=" ";}; {print "-e " $1}'`
}
eval "$(zoxide init --cmd cd bash)"

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/home/torrentofshame/Documents/savvysuit/pg-simonweizman-certs/google-cloud-sdk/path.bash.inc' ]; then . '/home/torrentofshame/Documents/savvysuit/pg-simonweizman-certs/google-cloud-sdk/path.bash.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/home/torrentofshame/Documents/savvysuit/pg-simonweizman-certs/google-cloud-sdk/completion.bash.inc' ]; then . '/home/torrentofshame/Documents/savvysuit/pg-simonweizman-certs/google-cloud-sdk/completion.bash.inc'; fi

export PYENV_ROOT="$HOME/.pyenv"
command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"

# Restart your shell for the changes to take effect.

# Load pyenv-virtualenv automatically by adding
# the following to ~/.bashrc:

#eval "$(pyenv virtualenv-init -)"
# TODO: Make a nicer way of setting all my PATH stuff!
export PATH="$PATH:/usr/local/share/dotnet:/home/torrentofshame/.dotnet/tools"
export DOTNET_ROOT="/usr/local/share/dotnet"
