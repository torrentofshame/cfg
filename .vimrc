" This is a bare-minimum .vimrc with no plugins that I can curl into remote machines
" that I'm ssh'ing into.
" Should be reachable at: https://cdn.weizman.us/.vimrc

set nocompatible

syntax on

if (has("termguicolors"))
  set termguicolors
endif

set completeopt=menu,menuone

let g:completion_matching_strategy_list = ['exact', 'substring', 'fuzzy']

set wildmode=longest,list,full
set wildmenu
" Ignore files
set wildignore+=*.pyc
set wildignore+=*_build/*
set wildignore+=**/coverage/*
set wildignore+=**/node_modules/*
set wildignore+=**/android/*
set wildignore+=**/ios/*
set wildignore+=**/.git/*

let maplocalleader = ","

set title

" Don't need neovim showing me what mode I'm in twice
set noshowmode

" vertical splits on right by default
set splitright

" line wrapping
set linebreak
set wrap
set textwidth=100
set formatoptions-=t
" Don't split a word when wrapping text
set lbr

tnoremap <Esc><Esc> <c-\><c-n>

filetype plugin indent on

" No modelines
set nomodeline

" Line Numbers
set number
set relativenumber

" auto set rnu on current split and absnum on others
augroup numbertoggle
    au!
    au BufEnter,FocusGained,InsertLeave * if &buftype ==# "terminal" || &buftype ==# "nofile" | echo "" | else | set rnu | endif
    au BufLeave,FocusLost,InsertEnter * if &buftype ==# "terminal" || &buftype ==# "nofile" | echo "" | else | set nornu | endif
augroup END

" Creates parent directories for new files if not exist.
function s:MkNonExDir(file, buf)
    if empty(getbufvar(a:buf, '&buftype')) && a:file!~#'\v^\w+\:\/'
        let dir=fnamemodify(a:file, ':h')
        if !isdirectory(dir)
            call mkdir(dir, 'p')
        endif
    endif
endfunction
augroup BWCCreateDir
    autocmd!
    autocmd BufWritePre * :call s:MkNonExDir(expand('<afile>'), +expand('<abuf>'))
augroup END

" Make vim confirm if I forget to save
set confirm

" Setup highlight for search
set incsearch
" Only highlight the first match
set nohlsearch

" Show @@@ in the last line if it's trucated
set display=truncate

" CD to the containing directory of the current file
map <leader>cd :cd %:p:h<CR>:pwd<CR>

" Show partial commands
set showcmd

" Backup files and send all mess files to the same place
" TODO: make use /tmp based off of $USER
"set backup
"set backupdir=$myvimtmpfiles
"set dir=/tmp/.neovim_tmp
"
"" Persistent undo
"set undofile
"set undodir=~/.config/nvim/.nvim_undo
"set undolevels=1000
"set undoreload=10000

" Add more matchedpairs
set matchpairs+=<:>
set matchpairs+=":"
set matchpairs+=':'
set matchpairs+=`:`

" Color column to visibly show 80 col for reference
set colorcolumn=80
hi ColorColumn ctermbg=238 guibg=#292929

if (has("termguicolors"))
  set termguicolors
endif

"===== Keybinds ====="

" print and yank from clipboard
nnoremap <leader>p "*p
nnoremap <leader>y "*y

" New line then clear (for when comments are auto added n shit)
nnoremap <leader>o o<Esc>0"_D
nnoremap <leader>O O<Esc>0"_D

nnoremap <leader>+ :vertical resize +5<CR>
nnoremap <leader>- :vertical resize -5<CR>

" Toggle cursorline
nnoremap <leader>tcl :ToggleCursorLine<CR>
command! ToggleCursorLine set cursorline!

" Delete line (keeping blank line)
nnoremap dl 0D

" Better tab commands
nnoremap th :tabfirst<CR>
nnoremap tk :tabnext<CR>
nnoremap tj :tabprev<CR>
nnoremap tl :tablast<CR>
nnoremap te :tabedit<Space>
nnoremap tn :tabnew<CR>
nnoremap td :tabclose<CR>
nnoremap tm :tabmove<Space>

" Tab nav using leader and num row
nnoremap <leader>1 1gt
nnoremap <leader>2 2gt
nnoremap <leader>3 3gt
nnoremap <leader>4 4gt
nnoremap <leader>5 5gt
nnoremap <leader>6 6gt
nnoremap <leader>7 7gt
nnoremap <leader>8 8gt
nnoremap <leader>9 9gt

" Redo last editor command
nmap <leader>. @:

" let j and k work well with line wrapping
nmap j gj
nmap k gk

" Yank to end of line
noremap Y y$

" better split navigation
nnoremap <A-S-H> <C-W><C-H>
nnoremap <A-S-J> <C-W><C-J>
nnoremap <A-S-K> <C-W><C-K>
nnoremap <A-S-L> <C-W><C-L>

" up and down 5 line at a time
nmap <C-j> 5j
vmap <C-j> 5j
nmap <C-k> 5k
vmap <C-k> 5k

" Moving lines
nnoremap <A-j> :m .+1<CR>==
nnoremap <A-k> :m .-2<CR>==
inoremap <A-j> <Esc>:m .+1<CR>==gi
inoremap <A-k> <Esc>:m .-2<CR>==gi
vnoremap <A-j> :m '>+1<CR>gv=gv
vnoremap <A-k> :m '<-2<CR>gv=gv

" mappings for s, %s, / and, ?
nnoremap / /\v
vnoremap / /\v
nnoremap ? ?\v
vnoremap ? ?\v
nmap <space> /
vmap <space> /
nmap <BS> ?
vmap <BS> ?

nmap <leader>ss :smagic/
nmap <leader>ms :%smagic/
nnoremap <leader>is :%s/\<<C-r><C-w>\>/

" Window (not-cursor) scrolling
nnoremap <Right> ;
nnoremap <Left> ,
nnoremap <Up> <c-y>
nnoremap <Down> <c-e>

" Toggle Spelling
nnoremap <leader>s :ToggleSpelling<CR>
command! ToggleSpelling silent set spell! spelllang=en
inoremap <C-l> <c-g>u<esc>[s1z=`]a<c-g>u
